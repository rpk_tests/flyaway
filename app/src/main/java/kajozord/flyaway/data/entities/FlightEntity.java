package kajozord.flyaway.data.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FlightEntity {

    @Expose
    @SerializedName("duration")
    private String mDuration;

    @Expose
    @SerializedName("time")
    private List<String> mTime;

    @Expose
    @SerializedName("timeUTC")
    private List<String> mTimeUTC;

    @Expose
    @SerializedName("infantsLeft")
    private int mInfantsLeft;

    @Expose
    @SerializedName("segments")
    private List<SegmentEntity> mSegments;

    @Expose
    @SerializedName("regularFare")
    private RegularFareEntity mRegularFare;

    @Expose
    @SerializedName("faresLeft")
    private int mFaresLeft;

    @Expose
    @SerializedName("flightNumber")
    private String mFlightNumber;

    @Expose
    @SerializedName("flightKey")
    private String mFlightKey;

    public String getDuration() {
        return mDuration;
    }

    public void setDuration(String duration) {
        mDuration = duration;
    }

    public List<String> getTime() {
        return mTime;
    }

    public void setTime(List<String> time) {
        mTime = time;
    }

    public int getInfantsLeft() {
        return mInfantsLeft;
    }

    public void setInfantsLeft(int infantsLeft) {
        mInfantsLeft = infantsLeft;
    }

    public List<SegmentEntity> getSegments() {
        return mSegments;
    }

    public void setSegments(List<SegmentEntity> segments) {
        mSegments = segments;
    }

    public RegularFareEntity getRegularFare() {
        return mRegularFare;
    }

    public void setRegularFare(RegularFareEntity regularFare) {
        mRegularFare = regularFare;
    }

    public List<String> getTimeUTC() {
        return mTimeUTC;
    }

    public void setTimeUTC(List<String> timeUTC) {
        mTimeUTC = timeUTC;
    }

    public int getFaresLeft() {
        return mFaresLeft;
    }

    public void setFaresLeft(int faresLeft) {
        mFaresLeft = faresLeft;
    }

    public String getFlightNumber() {
        return mFlightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        mFlightNumber = flightNumber;
    }

    public String getFlightKey() {
        return mFlightKey;
    }

    public void setFlightKey(String flightKey) {
        mFlightKey = flightKey;
    }

    @Override
    public String toString() {
        return "FlightEntity{" +
                "mDuration='" + mDuration + '\'' +
                ", mTime=" + mTime +
                ", mTimeUTC=" + mTimeUTC +
                ", mInfantsLeft=" + mInfantsLeft +
                ", mSegments=" + mSegments +
                ", mRegularFare=" + mRegularFare +
                ", mFaresLeft=" + mFaresLeft +
                ", mFlightNumber='" + mFlightNumber + '\'' +
                ", mFlightKey='" + mFlightKey + '\'' +
                '}';
    }

}
