package kajozord.flyaway.data.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TripEntity {
    
    @Expose
    @SerializedName("dates")
    private List<FlightDateEntity> mDates;

    @Expose
    @SerializedName("origin")
    private String mOrigin;

    @Expose
    @SerializedName("destinationName")
    private String mDestinationName;

    @Expose
    @SerializedName("destination")
    private String mDestination;

    @Expose
    @SerializedName("originName")
    private String mOriginName;

    public List<FlightDateEntity> getDates() {
        return mDates;
    }

    public void setDates(List<FlightDateEntity> dates) {
        mDates = dates;
    }

    public String getOrigin() {
        return mOrigin;
    }

    public void setOrigin(String origin) {
        mOrigin = origin;
    }

    public String getDestinationName() {
        return mDestinationName;
    }

    public void setDestinationName(String destinationName) {
        mDestinationName = destinationName;
    }

    public String getDestination() {
        return mDestination;
    }

    public void setDestination(String destination) {
        mDestination = destination;
    }

    public String getOriginName() {
        return mOriginName;
    }

    public void setOriginName(String originName) {
        mOriginName = originName;
    }

    @Override
    public String toString() {
        return "TripEntity{" +
                "mDates=" + mDates +
                ", mOrigin='" + mOrigin + '\'' +
                ", mDestinationName='" + mDestinationName + '\'' +
                ", mDestination='" + mDestination + '\'' +
                ", mOriginName='" + mOriginName + '\'' +
                '}';
    }

}
