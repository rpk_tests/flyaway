package kajozord.flyaway.data.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TripResponseEntity {

    @Expose
    @SerializedName("termsOfUse")
    private String mTermsOfUse;

    @Expose
    @SerializedName("currency")
    private String mCurrency;

    @Expose
    @SerializedName("currPrecision")
    private int mCurrPrecision;

    @Expose
    @SerializedName("trips")
    private List<TripEntity> mTrips;

    public String getTermsOfUse() {
        return mTermsOfUse;
    }

    public void setTermsOfUse(String termsOfUse) {
        mTermsOfUse = termsOfUse;
    }

    public String getCurrency() {
        return mCurrency;
    }

    public void setCurrency(String currency) {
        mCurrency = currency;
    }

    public int getCurrPrecision() {
        return mCurrPrecision;
    }

    public void setCurrPrecision(int currPrecision) {
        mCurrPrecision = currPrecision;
    }

    public List<TripEntity> getTrips() {
        return mTrips;
    }

    public void setTrips(List<TripEntity> trips) {
        mTrips = trips;
    }

    @Override
    public String toString() {
        return "TripResponse{" +
                "mTermsOfUse='" + mTermsOfUse + '\'' +
                ", mCurrency='" + mCurrency + '\'' +
                ", mCurrPrecision=" + mCurrPrecision +
                ", mTrips=" + mTrips +
                '}';
    }

}
