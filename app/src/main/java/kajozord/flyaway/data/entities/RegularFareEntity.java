package kajozord.flyaway.data.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RegularFareEntity {

    @Expose
    @SerializedName("fares")
    private List<FareEntity> mFares;

    @Expose
    @SerializedName("fareKey")
    private String mFareKey;

    @Expose
    @SerializedName("fareClass")
    private String mFareClass;

    public List<FareEntity> getFares() {
        return mFares;
    }

    public void setFares(List<FareEntity> fares) {
        mFares = fares;
    }

    public String getFareKey() {
        return mFareKey;
    }

    public void setFareKey(String fareKey) {
        mFareKey = fareKey;
    }

    public String getFareClass() {
        return mFareClass;
    }

    public void setFareClass(String fareClass) {
        mFareClass = fareClass;
    }

    @Override
    public String toString() {
        return "RegularFareEntity{" +
                "mFares=" + mFares +
                ", mFareKey='" + mFareKey + '\'' +
                ", mFareClass='" + mFareClass + '\'' +
                '}';
    }

}
