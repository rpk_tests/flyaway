package kajozord.flyaway.data.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FlightDateEntity {

    @Expose
    @SerializedName("flights")
    private List<FlightEntity> mFlights;

    @Expose
    @SerializedName("dateOut")
    private String mDateOut;

    public List<FlightEntity> getFlights() {
        return mFlights;
    }

    public void setFlights(List<FlightEntity> flights) {
        mFlights = flights;
    }

    public String getDateOut() {
        return mDateOut;
    }

    public void setDateOut(String dateOut) {
        mDateOut = dateOut;
    }

    @Override
    public String toString() {
        return "FlightDateEntity{" +
                "mFlights=" + mFlights +
                ", mDateOut='" + mDateOut + '\'' +
                '}';
    }
}
