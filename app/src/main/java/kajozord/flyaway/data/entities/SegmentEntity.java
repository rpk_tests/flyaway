package kajozord.flyaway.data.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SegmentEntity {

    @Expose
    @SerializedName("duration")
    private String mDuration;

    @Expose
    @SerializedName("time")
    private List<String> mTime;

    @Expose
    @SerializedName("origin")
    private String mOrigin;

    @Expose
    @SerializedName("timeUTC")
    private List<String> mTimeUTC;

    @Expose
    @SerializedName("flightNumber")
    private String mFlightNumber;

    @Expose
    @SerializedName("segmentNr")
    private int mSegmentNr;

    @Expose
    @SerializedName("destination")
    private String mDestination;

    public String getDuration() {
        return mDuration;
    }

    public void setDuration(String duration) {
        mDuration = duration;
    }

    public List<String> getTime() {
        return mTime;
    }

    public void setTime(List<String> time) {
        mTime = time;
    }

    public String getOrigin() {
        return mOrigin;
    }

    public void setOrigin(String origin) {
        mOrigin = origin;
    }

    public List<String> getTimeUTC() {
        return mTimeUTC;
    }

    public void setTimeUTC(List<String> timeUTC) {
        mTimeUTC = timeUTC;
    }

    public String getFlightNumber() {
        return mFlightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        mFlightNumber = flightNumber;
    }

    public int getSegmentNr() {
        return mSegmentNr;
    }

    public void setSegmentNr(int segmentNr) {
        mSegmentNr = segmentNr;
    }

    public String getDestination() {
        return mDestination;
    }

    public void setDestination(String destination) {
        mDestination = destination;
    }

    @Override
    public String toString() {
        return "SegmentEntity{" +
                "mDuration='" + mDuration + '\'' +
                ", mTime=" + mTime +
                ", mOrigin='" + mOrigin + '\'' +
                ", mTimeUTC=" + mTimeUTC +
                ", mFlightNumber='" + mFlightNumber + '\'' +
                ", mSegmentNr=" + mSegmentNr +
                ", mDestination='" + mDestination + '\'' +
                '}';
    }

}
