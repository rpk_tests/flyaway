package kajozord.flyaway.data.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StationsResponseEntity {

    @Expose
    @SerializedName("stations")
    private List<StationEntity> mStations;

    public List<StationEntity> getStations() {
        return mStations;
    }

    @Override
    public String toString() {
        return "StationsResponseEntity{" +
                "mStations=" + mStations +
                '}';
    }

}
