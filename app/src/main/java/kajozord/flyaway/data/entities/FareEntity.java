package kajozord.flyaway.data.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FareEntity {

    @Expose
    @SerializedName("amount")
    private float mAmount;

    @Expose
    @SerializedName("count")
    private int mCount;

    @Expose
    @SerializedName("hasPromoDiscount")
    private boolean mHasPromoDiscount;

    @Expose
    @SerializedName("discountInPercent")
    private int mDiscountInPercent;

    @Expose
    @SerializedName("type")
    private String mType;

    @Expose
    @SerializedName("publishedFare")
    private float mPublishedFare;

    @Expose
    @SerializedName("hasDiscount")
    private boolean mHasDiscount;

    public float getAmount() {
        return mAmount;
    }

    public void setAmount(float amount) {
        mAmount = amount;
    }

    public int getCount() {
        return mCount;
    }

    public void setCount(int count) {
        mCount = count;
    }

    public boolean getHasPromoDiscount() {
        return mHasPromoDiscount;
    }

    public void setHasPromoDiscount(boolean hasPromoDiscount) {
        mHasPromoDiscount = hasPromoDiscount;
    }

    public int getDiscountInPercent() {
        return mDiscountInPercent;
    }

    public void setDiscountInPercent(int discountInPercent) {
        mDiscountInPercent = discountInPercent;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public float getPublishedFare() {
        return mPublishedFare;
    }

    public void setPublishedFare(float publishedFare) {
        mPublishedFare = publishedFare;
    }

    public boolean getHasDiscount() {
        return mHasDiscount;
    }

    public void setHasDiscount(boolean hasDiscount) {
        mHasDiscount = hasDiscount;
    }

    @Override
    public String toString() {
        return "FareEntity{" +
                "mAmount=" + mAmount +
                ", mCount=" + mCount +
                ", mHasPromoDiscount=" + mHasPromoDiscount +
                ", mDiscountInPercent=" + mDiscountInPercent +
                ", mType='" + mType + '\'' +
                ", mPublishedFare='" + mPublishedFare + '\'' +
                ", mHasDiscount=" + mHasDiscount +
                '}';
    }
}
