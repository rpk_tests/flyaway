package kajozord.flyaway.data.mappers;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import kajozord.flyaway.data.entities.StationEntity;
import kajozord.flyaway.domain.Station;

public class StationEntityToStationMapper {

    private final static int INITIAL_LIST_CAPACITY = 50;

    @Inject
    public StationEntityToStationMapper() {

    }

    public @Nullable Station transform(@Nullable StationEntity stationEntity) {
        if (stationEntity == null) {
            return null;
        }

        Station station = new Station();
        station.setCode(stationEntity.getCode());
        station.setName(stationEntity.getName());
        station.setCountryCode(stationEntity.getCountryCode());
        station.setCountryName(stationEntity.getCountryName());

        return station;
    }

    public @NonNull List<Station> transform(@Nullable Collection<StationEntity> stationCollection) {
        List<Station> stationList = new ArrayList<>(INITIAL_LIST_CAPACITY);

        for (StationEntity stationEntity : Objects.requireNonNull(stationCollection)) {
            Station station = transform(stationEntity);
            if (station != null) {
                stationList.add(station);
            }
        }

        return stationList;
    }

}
