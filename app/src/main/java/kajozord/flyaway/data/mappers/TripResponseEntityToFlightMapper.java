package kajozord.flyaway.data.mappers;

import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import kajozord.flyaway.data.entities.FareEntity;
import kajozord.flyaway.data.entities.FlightDateEntity;
import kajozord.flyaway.data.entities.FlightEntity;
import kajozord.flyaway.data.entities.RegularFareEntity;
import kajozord.flyaway.data.entities.TripEntity;
import kajozord.flyaway.data.entities.TripResponseEntity;
import kajozord.flyaway.domain.Fare;
import kajozord.flyaway.domain.Flight;

public class TripResponseEntityToFlightMapper {

    private final static int INITIAL_LIST_CAPACITY = 50;

    @Inject
    public TripResponseEntityToFlightMapper() {

    }

    public @Nullable
    List<Flight> transform(TripResponseEntity tripResponseEntity) {
        if (tripResponseEntity == null) {
            return null;
        }

        List<Flight> flights = new ArrayList<>();
        for (TripEntity tripEntity : tripResponseEntity.getTrips()) {
            for (FlightDateEntity flightDate : tripEntity.getDates()) {
                for (FlightEntity flightEntity : flightDate.getFlights()) {
                    RegularFareEntity regularFareEntity = flightEntity.getRegularFare();
                    if (regularFareEntity == null) {
                        continue;
                    }

                    Flight flight = new Flight();
                    flight.setOrigin(tripEntity.getOrigin());
                    flight.setOriginName(tripEntity.getOriginName());
                    flight.setDestinationName(tripEntity.getDestinationName());
                    flight.setDestination(tripEntity.getDestination());
                    flight.setDuration(flightEntity.getDuration());
                    flight.setDateOut(flightEntity.getSegments().get(0).getTime().get(0));
                    flight.setFlightNumber(flightEntity.getFlightNumber());
                    flight.setCurrency(tripResponseEntity.getCurrency());
                    flight.setCurrPrecision(tripResponseEntity.getCurrPrecision());
                    flight.setFareClass(flightEntity.getRegularFare().getFareClass());
                    flight.setFares(transformFares(regularFareEntity));
                    flights.add(flight);
                }
            }
        }

        return flights;
    }

   private List<Fare> transformFares(RegularFareEntity regularFareEntity) {
        List<Fare> fares = new ArrayList<>();
        for (FareEntity fareEntity : regularFareEntity.getFares()) {
            Fare fare = new Fare();
            fare.setAmount(fareEntity.getAmount());
            fare.setPublishedFare(fareEntity.getPublishedFare());
            fare.setDiscountInPercent(fareEntity.getDiscountInPercent());
            fare.setHasDiscount(fareEntity.getHasDiscount());
            fare.setHasPromoDiscount(fareEntity.getHasPromoDiscount());
            fares.add(fare);
        }

        return fares;
    }

}
