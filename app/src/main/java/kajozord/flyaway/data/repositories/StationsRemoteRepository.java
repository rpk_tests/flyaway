package kajozord.flyaway.data.repositories;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import kajozord.flyaway.data.mappers.StationEntityToStationMapper;
import kajozord.flyaway.data.networking.StationsService;
import kajozord.flyaway.domain.Station;
import kajozord.flyaway.domain.repositories.StationRepository;

public class StationsRemoteRepository implements StationRepository {

    private final StationsService mStationsService;
    private final StationEntityToStationMapper mStationEntityDataMapper;

    @Inject
    public StationsRemoteRepository(StationEntityToStationMapper stationEntityDataMapper, StationsService
            stationsService) {
        mStationEntityDataMapper = stationEntityDataMapper;
        mStationsService = stationsService;
    }

    @Override
    public Observable<List<Station>> getAll() {
        return mStationsService
                .getAll()
                .map(apiResponse -> mStationEntityDataMapper.transform(apiResponse.getStations()));
    }

}
