package kajozord.flyaway.data.repositories;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import io.reactivex.Observable;
import kajozord.flyaway.data.mappers.TripResponseEntityToFlightMapper;
import kajozord.flyaway.data.networking.TripsService;
import kajozord.flyaway.domain.Flight;
import kajozord.flyaway.domain.repositories.FlightRepository;

public class FlightsRemoteRepository implements FlightRepository {

    public static final String FLIGHT_DATE_FORMAT = "yyyy-MM-dd";

    private final TripsService mTripsService;
    private final TripResponseEntityToFlightMapper mTripEntityDataMapper;

    @Inject
    public FlightsRemoteRepository(TripResponseEntityToFlightMapper tripEntityDataMapper, TripsService tripService) {
        mTripEntityDataMapper = tripEntityDataMapper;
        mTripsService = tripService;
    }

    @Override
    public Observable<List<Flight>> getAll(String origin, String destination, Calendar dateOut, String dateIn, int numberOfAdults, int numberOfTeens, int numberOfChildren) {
        return mTripsService.getAll(origin.toUpperCase(), destination.toUpperCase(), calendarToApiDateFormat(dateOut), dateIn, 0, 0, 0, 0, numberOfAdults, numberOfTeens, numberOfChildren, false)
                .map(apiResponse -> mTripEntityDataMapper.transform(apiResponse));
    }

    private String calendarToApiDateFormat(Calendar calendar) {
        return new SimpleDateFormat(FLIGHT_DATE_FORMAT, Locale.getDefault()).format(calendar.getTime());
    }

}
