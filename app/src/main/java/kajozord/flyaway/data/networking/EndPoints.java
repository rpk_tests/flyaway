package kajozord.flyaway.data.networking;

public class EndPoints {

    public static final String STATIONS_END_POINT = "https://tripstest.ryanair.com/";
    public static final String TRIPS_END_POINT = "https://sit-nativeapps.ryanair.com/";

}
