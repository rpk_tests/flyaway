package kajozord.flyaway.data.networking;

import io.reactivex.Observable;
import kajozord.flyaway.data.entities.StationsResponseEntity;
import retrofit2.http.GET;


public interface StationsService {

    @GET("static/stations.json")
    Observable<StationsResponseEntity> getAll();

}
