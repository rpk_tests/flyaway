package kajozord.flyaway.data.networking;

import io.reactivex.Observable;
import kajozord.flyaway.data.entities.TripResponseEntity;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface TripsService {

    @GET("api/v3/Availability?")
    Observable<TripResponseEntity> getAll(
            @Query("origin") String origin,
            @Query("destination") String destination,
            @Query("dateout") String dateOut,
            @Query("datein") String dateIn,
            @Query("flexdaysbeforeout") int flexDaysBeforeOut,
            @Query("flexdaysout") int flexDaysOut,
            @Query("flexdaysbeforein") int flexDaysBeforeIn,
            @Query("flexdaysin") int flexDaysIn,
            @Query("adt") int adt,
            @Query("teen") int teen,
            @Query("chd") int chd,
            @Query("roundtrip") boolean roundTrip);

}
