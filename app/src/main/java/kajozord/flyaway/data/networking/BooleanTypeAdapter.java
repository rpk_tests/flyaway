package kajozord.flyaway.data.networking;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;


class BooleanTypeAdapter implements JsonSerializer<Boolean>, JsonDeserializer<Boolean> {
    public JsonElement serialize(Boolean value, Type typeOfT, JsonSerializationContext context) {
        return new JsonPrimitive(value ? 1 : 0);
    }

    public Boolean deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        boolean value = false;
        if (json.isJsonPrimitive()) {
            if (json.getAsJsonPrimitive().isBoolean()) {
                value = json.getAsBoolean();
            } else if (json.getAsJsonPrimitive().isString()) {
                value = Integer.parseInt(json.getAsString()) != 0;
            } else if (json.getAsJsonPrimitive().isNumber()) {
                value = json.getAsInt() != 0;
            }
        } else {
            throw new JsonParseException("Incorrect type format received.");
        }
        return value;
    }
}

