package kajozord.flyaway.presentation.models;

import android.os.Parcel;
import android.os.Parcelable;

public class FlightModel implements Parcelable {

    private String mOrigin;
    private String mOriginName;
    private String mDestinationName;
    private String mDestination;
    private String mDuration;
    private String mFlightNumber;
    private long mDateOut;
    private String mOriginalDateOut;
    private int mInfantsLeft;
    private String mFareClass;
    private String mCurrency;
    private int mCurrPrecision;
    private int mDiscountInPercent;
    private float mPrice;

    public FlightModel() {

    }

    protected FlightModel(Parcel in) {
        mOrigin = in.readString();
        mOriginName = in.readString();
        mDestinationName = in.readString();
        mDestination = in.readString();
        mDuration = in.readString();
        mFlightNumber = in.readString();
        mDateOut = in.readLong();
        mOriginalDateOut = in.readString();
        mInfantsLeft = in.readInt();
        mFareClass = in.readString();
        mCurrency = in.readString();
        mCurrPrecision = in.readInt();
        mDiscountInPercent = in.readInt();
        mPrice = in.readFloat();
    }

    public String getOrigin() {
        return mOrigin;
    }

    public void setOrigin(String origin) {
        mOrigin = origin;
    }

    public String getOriginName() {
        return mOriginName;
    }

    public void setOriginName(String originName) {
        mOriginName = originName;
    }

    public String getDestinationName() {
        return mDestinationName;
    }

    public void setDestinationName(String destinationName) {
        mDestinationName = destinationName;
    }

    public String getDestination() {
        return mDestination;
    }

    public void setDestination(String destination) {
        mDestination = destination;
    }

    public String getDuration() {
        return mDuration;
    }

    public void setDuration(String duration) {
        mDuration = duration;
    }

    public String getFlightNumber() {
        return mFlightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        mFlightNumber = flightNumber;
    }

    public long getDateOut() {
        return mDateOut;
    }

    public void setDateOut(long dateOut) {
        mDateOut = dateOut;
    }

    public String getOriginalDateOut() {
        return mOriginalDateOut;
    }

    public void setOriginalDateOut(String originalDateOut) {
        mOriginalDateOut = originalDateOut;
    }

    public int getInfantsLeft() {
        return mInfantsLeft;
    }

    public void setInfantsLeft(int infantsLeft) {
        mInfantsLeft = infantsLeft;
    }

    public String getFareClass() {
        return mFareClass;
    }

    public void setFareClass(String fareClass) {
        mFareClass = fareClass;
    }

    public String getCurrency() {
        return mCurrency;
    }

    public void setCurrency(String currency) {
        mCurrency = currency;
    }

    public int getCurrPrecision() {
        return mCurrPrecision;
    }

    public void setCurrPrecision(int currPrecision) {
        mCurrPrecision = currPrecision;
    }


    public int getDiscountInPercent() {
        return mDiscountInPercent;
    }

    public void setDiscountInPercent(int discountInPercent) {
        mDiscountInPercent = discountInPercent;
    }

    public float getPrice() {
        return mPrice;
    }

    public void setPrice(float price) {
        mPrice = price;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mOrigin);
        dest.writeString(mOriginName);
        dest.writeString(mDestinationName);
        dest.writeString(mDestination);
        dest.writeString(mDuration);
        dest.writeString(mFlightNumber);
        dest.writeLong(mDateOut);
        dest.writeString(mOriginalDateOut);
        dest.writeInt(mInfantsLeft);
        dest.writeString(mFareClass);
        dest.writeString(mCurrency);
        dest.writeInt(mCurrPrecision);
        dest.writeInt(mDiscountInPercent);
        dest.writeFloat(mPrice);
    }

    public static final Creator<FlightModel> CREATOR = new Creator<FlightModel>() {
        @Override
        public FlightModel createFromParcel(Parcel source) {
            return new FlightModel(source);
        }

        @Override
        public FlightModel[] newArray(int size) {
            return new FlightModel[size];
        }
    };


    @Override
    public String toString() {
        return "FlightModel{" +
                "mOrigin='" + mOrigin + '\'' +
                ", mOriginName='" + mOriginName + '\'' +
                ", mDestinationName='" + mDestinationName + '\'' +
                ", mDestination='" + mDestination + '\'' +
                ", mDuration='" + mDuration + '\'' +
                ", mFlightNumber='" + mFlightNumber + '\'' +
                ", mDateOut=" + mDateOut +
                ", mOriginalDateOut='" + mOriginalDateOut + '\'' +
                ", mInfantsLeft=" + mInfantsLeft +
                ", mFareClass='" + mFareClass + '\'' +
                ", mCurrency='" + mCurrency + '\'' +
                ", mCurrPrecision=" + mCurrPrecision +
                ", mDiscountInPercent=" + mDiscountInPercent +
                ", mPrice=" + mPrice +
                '}';
    }
}
