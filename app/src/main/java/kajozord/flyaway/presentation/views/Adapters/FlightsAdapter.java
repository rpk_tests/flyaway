package kajozord.flyaway.presentation.views.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import kajozord.flyaway.R;
import kajozord.flyaway.presentation.models.FlightModel;

public class FlightsAdapter extends RecyclerView.Adapter<FlightsAdapter.UserViewHolder> {

    private final DateFormat mDateFormat;
    private final DateFormat mTimeFormat;

    public interface OnItemClickListener {
        void onUserItemClicked(FlightModel flight);
    }

    private List<FlightModel> mFlights;
    private final LayoutInflater mLayoutInflater;

    private OnItemClickListener mOnItemClickListener;

    public FlightsAdapter(Context context) {
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mFlights = Collections.emptyList();
        mDateFormat = android.text.format.DateFormat.getDateFormat(context);
        mTimeFormat = android.text.format.DateFormat.getTimeFormat(context);

    }

    @Override
    public int getItemCount() {
        return (mFlights != null) ? mFlights.size() : 0;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = mLayoutInflater.inflate(R.layout.item_flight, parent, false);
        return new UserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, final int position) {
        final FlightModel flightModel = mFlights.get(position);

        setDateOut(flightModel, holder.dateView);
        holder.flightNumberView.setText(flightModel.getFlightNumber());
        holder.durationView.setText(flightModel.getDuration());
        holder.priceView.setText(String.format("%s %s", flightModel.getPrice(), flightModel.getCurrency()));

        holder.itemView.setOnClickListener(v -> {
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onUserItemClicked(flightModel);
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setFlights(Collection<FlightModel> flights) {
        validateUsersCollection(flights);
        mFlights = (List<FlightModel>) flights;
        notifyDataSetChanged();
    }


    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    private void setDateOut(FlightModel flightModel, TextView view) {
        if (flightModel.getDateOut() != 0) {
            view.setText(String.format("%s %s", mDateFormat.format(flightModel.getDateOut()), mTimeFormat
                    .format(flightModel.getDateOut())));
        } else {
            view.setText(flightModel.getOriginalDateOut());
        }
    }

    private void validateUsersCollection(Collection<FlightModel> flights) {
        if (flights == null) {
            throw new IllegalArgumentException("The list cannot be null");
        }
    }

    static class UserViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text_flight_date)
        TextView dateView;

        @BindView(R.id.text_flight_number)
        TextView flightNumberView;

        @BindView(R.id.text_duration)
        TextView durationView;

        @BindView(R.id.text_price)
        TextView priceView;

        UserViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

