package kajozord.flyaway.presentation.views;

import java.util.List;

import kajozord.flyaway.presentation.models.FlightModel;

public interface FlightListView {

    List<FlightModel> getFlights();

    void populateList(List<FlightModel> flights);

    void showFlightInfo(String message);

    void setTitle(String title);

}
