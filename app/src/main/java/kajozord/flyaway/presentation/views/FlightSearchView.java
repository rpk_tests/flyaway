package kajozord.flyaway.presentation.views;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import kajozord.flyaway.domain.Station;
import kajozord.flyaway.presentation.models.FlightModel;

public interface FlightSearchView {

    void setFlightDate(Calendar calendar);

    String getFrom();

    String getTo();

    Calendar getFlightOut();

    int getAdultsCount();

    int getTeensCount();

    int getChildrenCount();

    void showError();

    void showError(String errorText);

    void showMessage(String message);

    void openFlightList(ArrayList<FlightModel> flights);

    void setStations(List<Station> stations);

}
