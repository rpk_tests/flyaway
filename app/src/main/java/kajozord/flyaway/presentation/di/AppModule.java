package kajozord.flyaway.presentation.di;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import kajozord.flyaway.data.executors.JobExecutor;
import kajozord.flyaway.data.networking.ApiFactory;
import kajozord.flyaway.data.networking.EndPoints;
import kajozord.flyaway.data.networking.StationsService;
import kajozord.flyaway.data.networking.TripsService;
import kajozord.flyaway.data.repositories.FlightsRemoteRepository;
import kajozord.flyaway.data.repositories.StationsRemoteRepository;
import kajozord.flyaway.domain.executors.PostExecutionThread;
import kajozord.flyaway.domain.executors.ThreadExecutor;
import kajozord.flyaway.domain.repositories.FlightRepository;
import kajozord.flyaway.domain.repositories.StationRepository;
import kajozord.flyaway.presentation.App;
import kajozord.flyaway.presentation.UIThread;

@Module
public class AppModule {

    private final App mApplication;

    private final ApiFactory mStationApiFactory = new ApiFactory(EndPoints.STATIONS_END_POINT);
    private final ApiFactory mTripsApiFactory = new ApiFactory(EndPoints.TRIPS_END_POINT);

    public AppModule(App app) {
        mApplication = app;
    }

    @Provides
    @Singleton
    App provideApplication() {
        return mApplication;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return mApplication;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return mApplication;
    }

    @Provides
    @Singleton
    ThreadExecutor provideThreadExecutor(JobExecutor jobExecutor) {
        return jobExecutor;
    }

    @Provides
    @Singleton
    PostExecutionThread providePostExecutionThread(UIThread uiThread) {
        return uiThread;
    }

    @Provides
    @Singleton
    StationRepository provideStationRepository(StationsRemoteRepository stationDataRepository) {
        return stationDataRepository;
    }

    @Provides
    @Singleton
    StationsService provideStationsService() {
        return mStationApiFactory.create(StationsService.class);
    }


    @Provides
    @Singleton
    FlightRepository provideFlightRepository(FlightsRemoteRepository flightDataRepository) {
        return flightDataRepository;
    }

    @Provides
    @Singleton
    TripsService provideFlightsService() {
        return mTripsApiFactory.create(TripsService.class);
    }

}
