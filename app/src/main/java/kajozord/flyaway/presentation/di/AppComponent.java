package kajozord.flyaway.presentation.di;

import javax.inject.Singleton;

import dagger.Component;
import kajozord.flyaway.presentation.App;
import kajozord.flyaway.presentation.activities.FlightListActivity;
import kajozord.flyaway.presentation.activities.FlightSearchActivity;

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {

    void inject(FlightSearchActivity flightSearchActivity);

    void inject(FlightListActivity flightListActivity);

    void inject(App app);

}

