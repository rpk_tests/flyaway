package kajozord.flyaway.presentation.activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.otaliastudios.autocomplete.Autocomplete;
import com.otaliastudios.autocomplete.AutocompleteCallback;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import kajozord.flyaway.R;
import kajozord.flyaway.domain.Station;
import kajozord.flyaway.presentation.models.FlightModel;
import kajozord.flyaway.presentation.presenters.FlightSearchPresenter;
import kajozord.flyaway.presentation.presenters.StationAutocompletePresenter;
import kajozord.flyaway.presentation.views.FlightSearchView;

public class FlightSearchActivity extends BaseActivity implements FlightSearchView {

    @Inject
    FlightSearchPresenter mFlightSearchPresenter;

    private Unbinder mUnBinder;

    @BindView(R.id.text_flight_date)
    TextView mFlightDateView;
    @BindView(R.id.input_flight_from)
    EditText mFromView;
    @BindView(R.id.input_flight_to)
    EditText mToView;
    @BindView(R.id.input_adults)
    EditText mAdultsView;
    @BindView(R.id.input_teens)
    EditText mTeensView;
    @BindView(R.id.input_children)
    EditText mChildrenView;

    private Calendar mCalendar = Calendar.getInstance();
    private StationAutocompletePresenter mFromAutocompletePresenter;
    private StationAutocompletePresenter mToAutocompletePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_search);
        init();
    }

    @Override
    public void onResume() {
        super.onResume();
        mFlightSearchPresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mFlightSearchPresenter.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mFlightSearchPresenter.destroy();
        mUnBinder.unbind();
    }

    private void init() {
        mUnBinder = ButterKnife.bind(this);
        getAppComponent().inject(this);
        mFlightSearchPresenter.setView(this);
        mFlightSearchPresenter.init();
        setFlightDate(mCalendar);
        initAutocomplete();
    }

    @Override
    public void setFlightDate(Calendar calendar) {
        mFlightDateView.setText(getLocalizedDateStringFromCalendar(calendar));
    }

    @OnClick(R.id.text_flight_date)
    public void onFlightDatePicker() {
        hideKeyboard();
        new DatePickerDialog(this,
                (datePicker, year, month, day) -> {
                    mCalendar.set(Calendar.YEAR, year);
                    mCalendar.set(Calendar.MONTH, month);
                    mCalendar.set(Calendar.DAY_OF_MONTH, day);
                    setFlightDate(mCalendar);
                },
                mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar
                .DAY_OF_MONTH))
                .show();
    }

    @OnClick(R.id.button_search)
    void onSearchFlight() {
        mFlightSearchPresenter.searchFlight();
    }

    @Override
    public String getFrom() {
        return getStringFromEditTextView(mFromView);
    }

    @Override
    public String getTo() {
        return getStringFromEditTextView(mToView);
    }

    @Override
    public Calendar getFlightOut() {
        return mCalendar;
    }

    @Override
    public int getAdultsCount() {
        return getIntFromEditTextView(mAdultsView);
    }

    @Override
    public int getTeensCount() {
        return getIntFromEditTextView(mTeensView);
    }

    @Override
    public int getChildrenCount() {
        return getIntFromEditTextView(mChildrenView);
    }

    @Override
    public void showError() {
        Toast.makeText(this, R.string.error_occurred, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showError(String errorText) {
        Toast.makeText(this, getString(R.string.error_occurred_with_description, errorText), Toast
                .LENGTH_LONG).show();
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void openFlightList(ArrayList<FlightModel> flights) {
        FlightListActivity.start(this, flights);
    }

    @Override
    public void setStations(List<Station> stations) {
        mFromAutocompletePresenter.setStations(stations);
        mToAutocompletePresenter.setStations(stations);
    }

    private void initAutocomplete() {
        mFromAutocompletePresenter = new StationAutocompletePresenter(this);
        mToAutocompletePresenter = new StationAutocompletePresenter(this);

        Autocomplete.<Station>on(mFromView)
                .with(6f)
                .with(new StationAutocompleteCallback())
                .with(mFromAutocompletePresenter)
                .with(new ColorDrawable(Color.WHITE))
                .build();

        Autocomplete.<Station>on(mToView)
                .with(6f)
                .with(new StationAutocompleteCallback())
                .with(mToAutocompletePresenter)
                .with(new ColorDrawable(Color.WHITE))
                .build();
    }

    private String getLocalizedDateStringFromCalendar(Calendar calendar) {
        java.text.DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(this);
        return dateFormat.format(calendar.getTime());
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = getCurrentFocus();
        if (view == null) {
            view = new View(this);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private class StationAutocompleteCallback implements AutocompleteCallback<Station> {
        @Override
        public boolean onPopupItemClicked(Editable editable, Station item) {
            editable.clear();
            editable.append(item.getCode());
            return true;
        }

        @Override
        public void onPopupVisibilityChanged(boolean shown) {
        }
    }

}
