package kajozord.flyaway.presentation.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import kajozord.flyaway.R;
import kajozord.flyaway.presentation.models.FlightModel;
import kajozord.flyaway.presentation.presenters.FlightListPresenter;
import kajozord.flyaway.presentation.views.Adapters.FlightsAdapter;
import kajozord.flyaway.presentation.views.Adapters.FlightsLayoutManager;
import kajozord.flyaway.presentation.views.FlightListView;

public class FlightListActivity extends BaseActivity implements FlightListView {

    public static final String ARG_FLIGHTS_KEY = "flights";

    @Inject
    FlightListPresenter mFlightListPresenter;

    @BindView(R.id.list_flights)
    RecyclerView mFlightListView;

    private FlightsAdapter mFlightsAdapter;
    private List<FlightModel> mFlights;
    private Unbinder mUnBinder;

    public static void start(Context context, ArrayList<FlightModel> flights) {
        context.startActivity(getCallIntent(context, flights));
    }

    public static Intent getCallIntent(Context context, ArrayList<FlightModel> flights) {
        Intent intent = new Intent(context, FlightListActivity.class);
        intent.putParcelableArrayListExtra(ARG_FLIGHTS_KEY, flights);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_list);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        parseArgs();

        init();
    }

    @Override
    public void onResume() {
        super.onResume();
        mFlightListPresenter.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mFlightListPresenter.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mFlightListPresenter.destroy();
        mUnBinder.unbind();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public List<FlightModel> getFlights() {
        return mFlights;
    }

    @Override
    public void populateList(List<FlightModel> flights) {
        if (flights != null) {
            mFlightsAdapter.setFlights(flights);
        }
    }

    @Override
    public void showFlightInfo(String message) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.flight_details_title)
                .setMessage(message)
                .setPositiveButton(getString(R.string.close), null)
                .create()
                .show();
    }

    @Override
    public void setTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    private void parseArgs() {
        if (getIntent() != null) {
            mFlights = getIntent().getParcelableArrayListExtra(ARG_FLIGHTS_KEY);
        }
    }

    private void init() {
        mUnBinder = ButterKnife.bind(this);
        getAppComponent().inject(this);
        setupRecyclerView();
        mFlightListPresenter.setView(this);
        mFlightListPresenter.init(mFlights);
    }

    private void setupRecyclerView() {
        mFlightsAdapter = new FlightsAdapter(this);
        mFlightsAdapter.setOnItemClickListener(flightModel -> {
            if (mFlightListPresenter != null && flightModel != null) {
                mFlightListPresenter.onFlightClicked(flightModel);
            }
        });
        mFlightListView.setLayoutManager(new FlightsLayoutManager(this));
        mFlightListView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.HORIZONTAL));
        mFlightListView.setAdapter(mFlightsAdapter);
    }

}
