package kajozord.flyaway.presentation.activities;

import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.EditText;

import kajozord.flyaway.presentation.App;
import kajozord.flyaway.presentation.di.AppComponent;

public abstract class BaseActivity extends AppCompatActivity {

    AppComponent getAppComponent() {
        return ((App) getApplication()).getAppComponent();
    }

    protected String getStringFromEditTextView(EditText view) {
        return view.getText().toString();
    }

    protected int getIntFromEditTextView(EditText view) {
        return parsePassengerNumber(view.getText().toString());
    }

    private int parsePassengerNumber(String number) {
        if (TextUtils.isEmpty(number) || !TextUtils.isDigitsOnly(number)) {
            return 0;
        }

        try {
            return Integer.parseInt(number);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return 0;
        }
    }

}
