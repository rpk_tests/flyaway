package kajozord.flyaway.presentation;

import android.app.Application;

import com.facebook.stetho.Stetho;

import kajozord.flyaway.BuildConfig;
import kajozord.flyaway.presentation.di.AppComponent;
import kajozord.flyaway.presentation.di.AppModule;
import kajozord.flyaway.presentation.di.DaggerAppComponent;

public class App extends Application {
    private AppComponent mAppComponent;

    public void onCreate() {
        super.onCreate();

        initStetho();
        initInjector();
    }

    private void initStetho() {
        if (!BuildConfig.DEBUG) {
            return;
        }

        Stetho.initializeWithDefaults(this);
    }

    private void initInjector() {
        mAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
        mAppComponent.inject(this);
    }

    public AppComponent getAppComponent() {
        return mAppComponent;
    }

}
