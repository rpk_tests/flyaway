package kajozord.flyaway.presentation.presenters;

import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import kajozord.flyaway.presentation.models.FlightModel;
import kajozord.flyaway.presentation.views.FlightListView;

public class FlightListPresenter implements Presenter<FlightListView> {

    private FlightListView mFlightListView;

    private List<FlightModel> mFlights;

    public void init(List<FlightModel> flights) {
        mFlights = flights;
        if (mFlights != null) {
            mFlightListView.populateList(mFlights);
            if (mFlights.size() != 0) {
                mFlightListView.setTitle(String.format("↗ %s  ✈  ↘ %s", mFlights.get(0).getOrigin(),
                        mFlights.get(0).getDestination()));
            }
        }
    }

    @Inject
    public FlightListPresenter() {

    }

    @Override
    public void setView(FlightListView view) {
        mFlightListView = view;
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void destroy() {

    }

    public void onFlightClicked(FlightModel flightModel) {
        String info = String.format(Locale.getDefault(), "↗ %s  ✈  ↘ %s\nInfants Left: %d\nFare " +
                "Class: %s\nDiscount: %s%%", flightModel.getOriginName(), flightModel.getDestinationName(), flightModel.getInfantsLeft(), flightModel.getFareClass(), flightModel.getDiscountInPercent());
        mFlightListView.showFlightInfo(info);
    }

}
