package kajozord.flyaway.presentation.presenters;

import java.util.List;

import javax.inject.Inject;

import kajozord.flyaway.data.repositories.FlightsRemoteRepository;
import kajozord.flyaway.domain.Flight;
import kajozord.flyaway.domain.Station;
import kajozord.flyaway.domain.repositories.StationRepository;
import kajozord.flyaway.domain.usecases.DefaultObserver;
import kajozord.flyaway.domain.usecases.GetFlightList;
import kajozord.flyaway.domain.usecases.GetStationList;
import kajozord.flyaway.presentation.mappers.FlightToFlightModelMapper;
import kajozord.flyaway.presentation.views.FlightSearchView;

public class FlightSearchPresenter implements Presenter<FlightSearchView> {

    private FlightSearchView mFlightSearchView;

    @Inject public StationRepository mStationRepository;
    @Inject public GetStationList mGetStationsUseCase;
    @Inject public FlightsRemoteRepository mTripRepository;
    @Inject public GetFlightList mGetFlightsUseCase;
    @Inject public FlightToFlightModelMapper mFlightToFlightUiItemMapper;

    @Inject
    public FlightSearchPresenter() {

    }

    @Override
    public void setView(FlightSearchView flightSearchView) {
        mFlightSearchView = flightSearchView;
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {
    }

    @Override
    public void destroy() {
        mGetStationsUseCase.dispose();
        mGetFlightsUseCase.dispose();
    }

    public void init() {
        mGetStationsUseCase.execute(new StationListObserver(), null);
    }

    public void searchFlight() {
        GetFlightList.Params params = new GetFlightList.Params.Builder()
                .from(mFlightSearchView.getFrom())
                .to(mFlightSearchView.getTo())
                .flightDate(mFlightSearchView.getFlightOut())
                .adults(mFlightSearchView.getAdultsCount())
                .teens(mFlightSearchView.getTeensCount())
                .children(mFlightSearchView.getChildrenCount())
                .build();

        mGetFlightsUseCase.execute(new FlightListObserver(), params);
    }

    private final class StationListObserver extends DefaultObserver<List<Station>> {

        @Override
        public void onComplete() {
        }

        @Override
        public void onError(Throwable e) {
            e.printStackTrace();
        }

        @Override
        public void onNext(List<Station> stations) {
            mFlightSearchView.setStations(stations);
        }

    }

    private final class FlightListObserver extends DefaultObserver<List<Flight>> {

        @Override
        public void onComplete() {

        }

        @Override
        public void onError(Throwable e) {
            mFlightSearchView.showError();
            e.printStackTrace();
        }

        @Override
        public void onNext(List<Flight> flights) {
            mFlightSearchView.openFlightList(mFlightToFlightUiItemMapper.transform(flights));
        }

    }

}
