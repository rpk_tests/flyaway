package kajozord.flyaway.presentation.presenters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.otaliastudios.autocomplete.AutocompletePresenter;
import com.otaliastudios.autocomplete.RecyclerViewPresenter;

import java.util.ArrayList;
import java.util.List;

import kajozord.flyaway.domain.Station;

public class StationAutocompletePresenter extends RecyclerViewPresenter<Station> {

    private AutocompleteAdapter mAdapter;
    private List<Station> mStations;

    public StationAutocompletePresenter(Context context) {
        super(context);
    }

    @Override
    protected AutocompletePresenter.PopupDimensions getPopupDimensions() {
        AutocompletePresenter.PopupDimensions dims = new AutocompletePresenter.PopupDimensions();
        dims.width = ViewGroup.LayoutParams.MATCH_PARENT;
        dims.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        return dims;
    }

    @Override
    protected RecyclerView.Adapter instantiateAdapter() {
        mAdapter = new AutocompleteAdapter();
        return mAdapter;
    }

    @Override
    protected void onQuery(@Nullable CharSequence query) {
        if (mStations == null) {
            return;
        }

        if (TextUtils.isEmpty(query)) {
            mAdapter.setStations(mStations);
        } else {
            List<Station> list = new ArrayList<>();
            for (Station station : mStations) {
                if (contains(query, station.getCountryName()) ||
                        contains(query, station.getCode()) ||
                        contains(query, station.getCountryCode()) ||
                        contains(query, station.getName())) {
                    list.add(station);
                }
            }
            mAdapter.setStations(list);
        }
        mAdapter.notifyDataSetChanged();
    }

    private boolean contains(CharSequence query, String compareTo) {
        return  compareTo.toLowerCase().contains(query.toString().toLowerCase());
    }

    public void setStations(List<Station> stations) {
        mStations = stations;
    }

    class AutocompleteAdapter extends RecyclerView.Adapter<AutocompleteAdapter.Holder> {

        private List<Station> mStations;

        class Holder extends RecyclerView.ViewHolder {
            private View root;
            private TextView text;

            Holder(View itemView) {
                super(itemView);
                root = itemView;
                text = itemView.findViewById(android.R.id.text1);
            }
        }

        void setStations(List<Station> stations) {
            mStations = stations;
        }

        @Override
        public int getItemCount() {
            return (isEmpty()) ? 1 : mStations.size();
        }

        @Override
        public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new Holder(LayoutInflater.from(getContext()).inflate(android.R.layout.simple_list_item_1, parent, false));
        }

        private boolean isEmpty() {
            return mStations == null || mStations.isEmpty();
        }

        @Override
        public void onBindViewHolder(Holder holder, int position) {
            if (isEmpty()) {
                holder.text.setText("");
                holder.root.setOnClickListener(null);
                return;
            }
            final Station station = mStations.get(position);

            holder.text.setText(String.format("%s %s %s", station.getName(), station.getCountryName(),
                    station.getCode()));
            holder.root.setOnClickListener(v -> dispatchClick(station));
        }
    }
}
