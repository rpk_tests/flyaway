package kajozord.flyaway.presentation.mappers;

import android.support.annotation.Nullable;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import kajozord.flyaway.domain.Fare;
import kajozord.flyaway.domain.Flight;
import kajozord.flyaway.presentation.models.FlightModel;

public class FlightToFlightModelMapper {

    public static final String SERVER_DATE_STRING_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";

    @Inject
    public FlightToFlightModelMapper() {
    }

    public @Nullable
    ArrayList<FlightModel> transform(List<Flight> flights) {
        if (flights == null) {
            return null;
        }

        ArrayList<FlightModel> flightItems = new ArrayList<>();
        for (Flight flight : flights) {
            FlightModel flightUiItem = new FlightModel();
            flightUiItem.setOrigin(flight.getOrigin());
            flightUiItem.setOriginName(flight.getOriginName());
            flightUiItem.setDestinationName(flight.getDestinationName());
            flightUiItem.setDestination(flight.getDestination());
            flightUiItem.setDuration(flight.getDuration());
            flightUiItem.setDateOut(transformDate(flight.getDateOut()));
            flightUiItem.setOriginalDateOut(flight.getDateOut());
            flightUiItem.setFlightNumber(flight.getFlightNumber());
            flightUiItem.setCurrency(flight.getCurrency());
            flightUiItem.setCurrPrecision(flight.getCurrPrecision());
            flightUiItem.setFareClass(flight.getFareClass());
            flightUiItem.setInfantsLeft(flight.getInfantsLeft());
            flightUiItem.setDiscountInPercent(flight.getFares().get(0).getDiscountInPercent());

            float price = 0;
            for (Fare fare : flight.getFares()) {
                price += fare.getAmount();
            }
            flightUiItem.setPrice(price);

            flightItems.add(flightUiItem);
        }

        return flightItems;
    }

    private long transformDate(String serverDate) {
        try {
            DateFormat format = new SimpleDateFormat(SERVER_DATE_STRING_FORMAT, Locale.getDefault());
            Date date = format.parse(serverDate.replace("T", " "));
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return 0;
    }

}
