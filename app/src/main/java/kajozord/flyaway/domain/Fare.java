package kajozord.flyaway.domain;

public class Fare {

    private float mAmount;
    private float mPublishedFare;
    private boolean mHasPromoDiscount;
    private boolean mHasDiscount;
    private int mDiscountInPercent;

    public float getAmount() {
        return mAmount;
    }

    public void setAmount(float amount) {
        mAmount = amount;
    }

    public float getPublishedFare() {
        return mPublishedFare;
    }

    public void setPublishedFare(float publishedFare) {
        mPublishedFare = publishedFare;
    }

    public boolean isHasPromoDiscount() {
        return mHasPromoDiscount;
    }

    public void setHasPromoDiscount(boolean hasPromoDiscount) {
        mHasPromoDiscount = hasPromoDiscount;
    }

    public boolean isHasDiscount() {
        return mHasDiscount;
    }

    public void setHasDiscount(boolean hasDiscount) {
        mHasDiscount = hasDiscount;
    }

    public int getDiscountInPercent() {
        return mDiscountInPercent;
    }

    public void setDiscountInPercent(int discountInPercent) {
        mDiscountInPercent = discountInPercent;
    }

    @Override
    public String toString() {
        return "Fare{" +
                "mAmount=" + mAmount +
                ", mPublishedFare=" + mPublishedFare +
                ", mHasPromoDiscount=" + mHasPromoDiscount +
                ", mHasDiscount=" + mHasDiscount +
                ", mDiscountInPercent=" + mDiscountInPercent +
                '}';
    }

}
