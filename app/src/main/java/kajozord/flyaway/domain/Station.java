package kajozord.flyaway.domain;

import java.util.List;

public class Station {

    private String mCode;
    private String mName;
    private String mAlternateName;
    private List<String> mAlias;
    private String mCountryCode;
    private String mCountryName;
    private String mCountryAlias;
    private int mCountryGroupCode;
    private String mCountryGroupName;
    private String mTimeZoneCode;
    private String mLatitude;
    private String mLongitude;
    private boolean mMobileBoardingPass;
    private List<Market> mMarkets;
    private String mNotices;

    public String getCode() {
        return mCode;
    }

    public void setCode(String code) {
        mCode = code;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getAlternateName() {
        return mAlternateName;
    }

    public void setAlternateName(String alternateName) {
        mAlternateName = alternateName;
    }

    public List<String> getAlias() {
        return mAlias;
    }

    public void setAlias(List<String> alias) {
        mAlias = alias;
    }

    public String getCountryCode() {
        return mCountryCode;
    }

    public String getCountryName() {
        return mCountryName;
    }

    public void setCountryName(String countryName) {
        mCountryName = countryName;
    }

    public void setCountryCode(String countryCode) {
        mCountryCode = countryCode;
    }

    public String getCountryAlias() {
        return mCountryAlias;
    }

    public void setCountryAlias(String countryAlias) {
        mCountryAlias = countryAlias;
    }

    public int getCountryGroupCode() {
        return mCountryGroupCode;
    }

    public void setCountryGroupCode(int countryGroupCode) {
        mCountryGroupCode = countryGroupCode;
    }

    public String getCountryGroupName() {
        return mCountryGroupName;
    }

    public void setCountryGroupName(String countryGroupName) {
        mCountryGroupName = countryGroupName;
    }

    public String getTimeZoneCode() {
        return mTimeZoneCode;
    }

    public void setTimeZoneCode(String timeZoneCode) {
        mTimeZoneCode = timeZoneCode;
    }

    public String getLatitude() {
        return mLatitude;
    }

    public void setLatitude(String latitude) {
        mLatitude = latitude;
    }

    public String getLongitude() {
        return mLongitude;
    }

    public void setLongitude(String longitude) {
        mLongitude = longitude;
    }

    public boolean isMobileBoardingPass() {
        return mMobileBoardingPass;
    }

    public void setMobileBoardingPass(boolean mobileBoardingPass) {
        mMobileBoardingPass = mobileBoardingPass;
    }

    public List<Market> getMarkets() {
        return mMarkets;
    }

    public void setMarkets(List<Market> markets) {
        mMarkets = markets;
    }

    public String getNotices() {
        return mNotices;
    }

    public void setNotices(String notices) {
        mNotices = notices;
    }

    public class Market {
        private String mCode;
        private String mGroup;

        public String getCode() {
            return mCode;
        }

        public void setCode(String code) {
            mCode = code;
        }

        public String getGroup() {
            return mGroup;
        }

        public void setGroup(String group) {
            mGroup = group;
        }

    }

    @Override
    public String toString() {
        return "Station{" +
                "mCode='" + mCode + '\'' +
                ", mName='" + mName + '\'' +
                ", mAlternateName='" + mAlternateName + '\'' +
                ", mAlias=" + mAlias +
                ", mCountryCode='" + mCountryCode + '\'' +
                ", mCountryName='" + mCountryName + '\'' +
                ", mCountryAlias='" + mCountryAlias + '\'' +
                ", mCountryGroupCode=" + mCountryGroupCode +
                ", mCountryGroupName='" + mCountryGroupName + '\'' +
                ", mTimeZoneCode='" + mTimeZoneCode + '\'' +
                ", mLatitude='" + mLatitude + '\'' +
                ", mLongitude='" + mLongitude + '\'' +
                ", mMobileBoardingPass=" + mMobileBoardingPass +
                ", mMarkets=" + mMarkets +
                ", mNotices='" + mNotices + '\'' +
                '}';
    }
}
