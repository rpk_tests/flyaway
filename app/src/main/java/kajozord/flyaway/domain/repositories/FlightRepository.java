package kajozord.flyaway.domain.repositories;

import java.util.Calendar;
import java.util.List;

import io.reactivex.Observable;
import kajozord.flyaway.domain.Flight;

public interface FlightRepository {

    Observable<List<Flight>> getAll(String origin, String destination, Calendar dateOut, String dateIn, int numberOfAdults, int numberOfTeens, int numberOfChildren);

}
