package kajozord.flyaway.domain.repositories;

import java.util.List;

import io.reactivex.Observable;
import kajozord.flyaway.domain.Station;

public interface StationRepository {

    Observable<List<Station>> getAll();

}
