package kajozord.flyaway.domain.usecases;

import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import kajozord.flyaway.domain.Flight;
import kajozord.flyaway.domain.executors.PostExecutionThread;
import kajozord.flyaway.domain.executors.ThreadExecutor;
import kajozord.flyaway.domain.repositories.FlightRepository;

public class GetFlightList extends BaseUseCase<List<Flight>, GetFlightList.Params> {

    private final FlightRepository mFlights;

    @Inject
    GetFlightList(FlightRepository flights, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        mFlights = flights;
    }

    @Override
    Observable<List<Flight>> buildUseCaseObservable(GetFlightList.Params params) {
        return mFlights.getAll(params.origin, params.destination, params.flightDate, "", params.numberOfAdults, params.numberOfTeens, params.numberOfChildren);
    }

    public static final class Params {

        private final String origin;
        private final String destination;
        private final Calendar flightDate;
        private final int numberOfAdults;
        private final int numberOfTeens;
        private final int numberOfChildren;

        private Params(String origin, String destination, Calendar flightDate, int numberOfAdults, int numberOfTeens, int numberOfChildren) {
            this.origin = origin;
            this.destination = destination;
            this.flightDate = flightDate;
            this.numberOfAdults = numberOfAdults;
            this.numberOfTeens = numberOfTeens;
            this.numberOfChildren = numberOfChildren;
        }

        public static class Builder {
            private String mFrom;
            private String mTo;
            private Calendar mFlightDate;
            private int mAdults;
            private int mTeens;
            private int mChildren;

            public Builder from(String from) {
                mFrom = from;
                return this;
            }

            public Builder to(String to) {
                mTo = to;
                return this;
            }

            public Builder flightDate(Calendar flightDate) {
                mFlightDate = flightDate;
                return this;
            }

            public Builder adults(int adults) {
                mAdults = adults;
                return this;
            }

            public Builder teens(int teens) {
                mTeens = teens;
                return this;
            }

            public Builder children(int children) {
                mChildren = children;
                return this;
            }

            public Params build() {
                return new Params(mFrom, mTo, mFlightDate, mAdults, mTeens, mChildren);
            }

        }

    }

}
