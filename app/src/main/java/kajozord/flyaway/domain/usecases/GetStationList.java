package kajozord.flyaway.domain.usecases;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import kajozord.flyaway.domain.Station;
import kajozord.flyaway.domain.executors.PostExecutionThread;
import kajozord.flyaway.domain.executors.ThreadExecutor;
import kajozord.flyaway.domain.repositories.StationRepository;

public class GetStationList extends BaseUseCase<List<Station>, Void> {

    private final StationRepository mStations;

    @Inject
    GetStationList(StationRepository stations, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        mStations = stations;
    }

    @Override
    Observable<List<Station>> buildUseCaseObservable(Void unused) {
        return mStations.getAll();
    }

}
