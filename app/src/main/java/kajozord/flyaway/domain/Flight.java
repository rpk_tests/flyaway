package kajozord.flyaway.domain;

import java.util.List;

public class Flight {

    private String mOrigin;
    private String mOriginName;
    private String mDestinationName;
    private String mDestination;
    private String mDuration;
    private String mFlightNumber;
    private String mDateOut;
    private int mInfantsLeft;
    private String mFareClass;
    private String mCurrency;
    private int mCurrPrecision;
    private List<Fare> mFares;

    public String getOrigin() {
        return mOrigin;
    }

    public void setOrigin(String origin) {
        mOrigin = origin;
    }

    public String getOriginName() {
        return mOriginName;
    }

    public void setOriginName(String originName) {
        mOriginName = originName;
    }

    public String getDestinationName() {
        return mDestinationName;
    }

    public void setDestinationName(String destinationName) {
        mDestinationName = destinationName;
    }

    public String getDestination() {
        return mDestination;
    }

    public void setDestination(String destination) {
        mDestination = destination;
    }

    public String getDuration() {
        return mDuration;
    }

    public void setDuration(String duration) {
        mDuration = duration;
    }

    public String getFlightNumber() {
        return mFlightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        mFlightNumber = flightNumber;
    }

    public String getDateOut() {
        return mDateOut;
    }

    public void setDateOut(String dateOut) {
        mDateOut = dateOut;
    }

    public int getInfantsLeft() {
        return mInfantsLeft;
    }

    public void setInfantsLeft(int infantsLeft) {
        mInfantsLeft = infantsLeft;
    }

    public String getFareClass() {
        return mFareClass;
    }

    public void setFareClass(String fareClass) {
        mFareClass = fareClass;
    }

    public String getCurrency() {
        return mCurrency;
    }

    public void setCurrency(String currency) {
        mCurrency = currency;
    }

    public int getCurrPrecision() {
        return mCurrPrecision;
    }

    public void setCurrPrecision(int currPrecision) {
        mCurrPrecision = currPrecision;
    }

    public List<Fare> getFares() {
        return mFares;
    }

    public void setFares(List<Fare> fares) {
        mFares = fares;
    }

    @Override
    public String toString() {
        return "Flight{" +
                "mOrigin='" + mOrigin + '\'' +
                ", mOriginName='" + mOriginName + '\'' +
                ", mDestinationName='" + mDestinationName + '\'' +
                ", mDestination='" + mDestination + '\'' +
                ", mDuration='" + mDuration + '\'' +
                ", mFlightNumber='" + mFlightNumber + '\'' +
                ", mDateOut='" + mDateOut + '\'' +
                ", mInfantsLeft=" + mInfantsLeft +
                ", mFareClass='" + mFareClass + '\'' +
                ", mCurrency='" + mCurrency + '\'' +
                ", mCurrPrecision=" + mCurrPrecision +
                ", mFares=" + mFares +
                '}';
    }
}
