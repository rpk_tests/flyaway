package kajozord.flyaway.domain;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FlightTest {

    private static final String FAKE_ORIGIN = "DUB";

    private Flight flight;

    @Before
    public void setUp() {
        flight = new Flight();
        flight.setOrigin(FAKE_ORIGIN);
    }

    @Test
    public void testFlightSetterHappyCase() {
        final String origin = flight.getOrigin();

        assertEquals(FAKE_ORIGIN, origin);
    }

}